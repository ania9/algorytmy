# before python run I set in shell:
# export PYTHONHASHSEED=0
# to have deterministic hash results

def wHash(str):
    return hash(str)

def dHash(str):
    sum = 0
    for char in str:
        sum = sum + 111*ord(char)
    return sum

def sHash(str):
    return len(str)

def makeTable(size):
    listOfLists = []
    for i in range (0,size):
        listOfLists.append([])
    return listOfLists

def readWords():
    words = []
    with open('words.txt', 'r') as file:
        # reading each line
        for word in file:
            words.append(word.strip())
    return words

def testHashFunc(hashFunc,size):
    table = makeTable(size)
    # calculate hashes
    for word in words[0:size*2]:
        hashVal = hashFunc(word) % size
        table[hashVal].append(word)

    # set initial counters values
    empty = 0
    maxLen = 0

    for i in range(size):
        listLen=len(table[i])
        if  listLen == 0:
            empty+=1
            continue
        if maxLen < listLen:
            maxLen = listLen

    avgLen = (size*2)/(size-empty)

    print("Table size:",size)
    print("Hash func used:",hashFunc.__name__)
    print("Empty:",empty)
    print("MaxLen:",maxLen)
    print("AvgLen:%.2f" % avgLen)
    print("\n")

# main

# small file can read whole to memory
words = readWords()

# W17, D17, S17, W1031, D1031, S1031, W1024, D1024, S1024
hashFuncs=[wHash,dHash,sHash]
sizeList=[17,1031,1024]

for size in sizeList:
    for f in hashFuncs:
        testHashFunc(f,size)

# results
# Table size: 17
# Hash func used: wHash
# Empty: 1
# MaxLen: 5
# AvgLen:2.12
#
#
# Table size: 17
# Hash func used: dHash
# Empty: 1
# MaxLen: 4
# AvgLen:2.12
#
#
# Table size: 17
# Hash func used: sHash
# Empty: 11
# MaxLen: 10
# AvgLen:5.67
#
#
# Table size: 1031
# Hash func used: wHash
# Empty: 148
# MaxLen: 8
# AvgLen:2.34
#
#
# Table size: 1031
# Hash func used: dHash
# Empty: 325
# MaxLen: 13
# AvgLen:2.92
#
#
# Table size: 1031
# Hash func used: sHash
# Empty: 1016
# MaxLen: 285
# AvgLen:137.47
#
#
# Table size: 1024
# Hash func used: wHash
# Empty: 144
# MaxLen: 8
# AvgLen:2.33
#
#
# Table size: 1024
# Hash func used: dHash
# Empty: 325
# MaxLen: 13
# AvgLen:2.93
#
#
# Table size: 1024
# Hash func used: sHash
# Empty: 1009
# MaxLen: 283
# AvgLen:136.53


# A1. Wyniki dla list 1031 i 1024 są prawie identyczne
# A2. Oczywiscie , kolejność od najlepszej wHash, dHash, sHash
