#!/usr/bin/env python
 

SIZE_M=13

def h1(k):
    return k % SIZE_M

def h2(k):
    return 1+ (k % (SIZE_M-2))

# hashF - hash function
# table - array
# k - value to be inserted
# SIZE_M - array size
def insertValue(hashF,table,k):
    print("Insert value:",k,"position:",end=" ")
    for i in range(SIZE_M):
        pos = hashF(k,i)
        print(pos,end=" ")
        if table[pos] is None:
            table[pos] = k
            print()
            break

def searchValue(hashF,table,k):
    for i in range(SIZE_M):
        pos = hashF(k,i)
        if table[pos] is not None:
            if table[pos] == k:
                return pos
    return None


def deleteValue(hashF,table,k):
    pos = searchValue(hashF,table,k)
    if pos is not None:
        table[pos] = None

def initializeTable(exampleData, hashF):
    table = [None] * SIZE_M
    for v in exampleData:
        insertValue(hashF, table, v)
    return table


# A
def hashLinear(k,i):
    return (h1(k)+i) % SIZE_M




# A
print('=Section A')


T = initializeTable([6, 19, 28, 41, 54],hashLinear)

print('Table T after insert:',T)
deleteValue(hashLinear,T,41)
print('Table T after delete:',T)
pos54 = searchValue(hashLinear,T,54)
pos32 = searchValue(hashLinear,T,32)
print('Value 54 found at position',pos54)
print('Value 32 found at position',pos32)

print("# end of Section A")
print()

#B
print("=section B")
def hashSquare(k,i):
    return (h1(k)+i*i) % SIZE_M


T = initializeTable([ 6, 19, 28, 41, 54, 67],hashSquare)
print("Table T after insert:", T)
deleteValue(hashSquare,T,54)
print("Table T after delete:",T)
pos67= searchValue(hashSquare,T,67)
print('Value 67 found at position',pos67)
print("# end of section B")


#C
print("=Section C")
def hashDouble(k,i):
    return  (h1(k)+i*h2(k))% SIZE_M
T = initializeTable([50,69,72,79,98],hashDouble)
print(T)
insertValue(hashDouble,T,14)
print("Table T after insert:", T)
deleteValue(hashDouble,T,98)
print("Table T after delete:",T)
pos14 = searchValue(hashDouble,T,14)
print('Value 14 found at position',pos14)
print("# end of section C")
