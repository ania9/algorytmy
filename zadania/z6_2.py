#!/usr/bin/env python

# before python run I set in shell:
# export PYTHONHASHSEED=0
# to have deterministic built-in hash results

def name_stat(s):
    count, name = s.strip().split(' ')
    return dict(ilosc=count,nazwisko=name)

def h1(size,k):
    return k % size

def h2(size,k):
    return 1+ (k % (size-2))

def hash_linear(size,k,i):
    return (h1(size,k) +i) % size

def hash_square(size,k,i):
    return (h1(size,k)+i*i) % size

def hash_double(size,k,i):
    return  (h1(size,k)+i*h2(size,k)) % size

def name_to_number(rec):
    return hash(rec['nazwisko'])

# hashF - hash function
# table - array
# k - value to be inserted
# SIZE_M - array size
def insert_value(hashF,table,rec,verbose):
    num_of_tries = 0
    size = len(table)
    if verbose:
        print("Insert value:",rec,"position:",end=" ")
    for i in range(size):
        pos = hashF(size,name_to_number(rec),i)
        num_of_tries += 1
        if verbose:
            print(pos,end=" ")
        if table[pos] is None:
            table[pos] = rec
            if verbose:
                print()
            return num_of_tries
    return num_of_tries

def read_data(f_name,limit):
    table = []
    with open(f_name, 'r') as file:
        # reading each line
        i = 0
        for line in file:
            if limit != -1 and i >= limit:
                break
            table.append(name_stat(line))
            i += 1
    return table



# test if works ok
size = 7
data_len = 4
hash_func=hash_linear
verbose = True
data= read_data('nazwiska.txt',data_len)
table = [None] * size
sum_of_tries = 0
for rec in data:
    sum_of_tries += insert_value(hash_func,table,rec,verbose)

average_tries = sum_of_tries/data_len
print('Table size: {}'.format(size))
print("Data len: {len:.0f}%".format(len=round(data_len/size*100,0)))
print("Hash function: {}".format(hash_func.__name__))
# 
print('Average number of tries: {avg:.2f}'.format(avg=average_tries))
if verbose:
    print(table)
print()  


# Tests
hash_funcs=[hash_linear,hash_square,hash_double]
percents=[50,70,90]
for hash_func in hash_funcs: 
    for percent in percents:
        size = 9349
        data_len = int(percent/100 * size)
        verbose = False
        data= read_data('nazwiska.txt',data_len)
        table = [None] * size
        sum_of_tries = 0
        for rec in data:
            sum_of_tries += insert_value(hash_func,table,rec,verbose)

        average_tries = sum_of_tries/data_len
        print('Table size: {}'.format(size))
        print("Data len: {len:.0f}%".format(len=round(data_len/size*100,0)))
        print("Hash function: {}".format(hash_func.__name__))
        # 
        print('Average number of tries: {avg:.2f}'.format(avg=average_tries))
        if verbose:
            print(table)
        print()  